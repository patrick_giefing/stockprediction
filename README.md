# Stock prediction (sentiment analysis + ARIMA)

## Stack overview

![stack overview](./presentation/img/screenshot/stack-overview.png)

## Message flow

![message flow](./presentation/img/screenshot/message-flow.png)

![message flow](./presentation/img/screenshot/docker-compose.png)
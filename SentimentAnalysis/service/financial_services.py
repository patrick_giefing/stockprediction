import rest.client.yahoo_finance as yahoo
import rest.client.converter.yahoo_finance_response_converters as yahoo_converters
import rest.client.reddit as reddit
import util.mongodb_utils as mongo
import util.kafka_utils as kafka
from elasticsearch import Elasticsearch
import json
import util.rabbitmq_utils as rabbitmq
import util.postgres_utils as postgres
import rest.client.twitter as twitter
import util.hash_utils as hash_utils
import config.config as config
import config.messages as messages
import message.message_enrich as message_enrich
import pandas as pd
import rest.service.dict_list_html_converter as dict_list_html_converter

elasticsearch = Elasticsearch(hosts=config.get_elasticsearch_host())


def queue_symbol_action(symbol: str, action: str):
    """
    Queue a symbol action for async processing
    :param symbol: stock ticker symbol, e.g. TSLA
    :param action: one of: chart, summary, news, tweets, reddit
    :return:
    """
    rabbitmq.publish("symbol-action", "all", json.dumps({'symbol': symbol, 'action': action}))


def fetch_symbol_reddit_process_sync(symbol: str, max_results: int = 10, date: str = None):
    """
    Fetches subreddit-posts, evaluates the sentiments and sends them to MongoDB/ElasticSearch
    :param symbol: stock ticker symbol, e.g. TSLA
    :param max_results:
    :param date:
    :return:
    """
    subreddits = reddit.get_symbol_subreddits(symbol, max_results, date)
    for subreddit in subreddits:
        try:
            subreddit_elastic = message_enrich.enrich_reddit_message_with_sentiment_symbol_date(subreddit)
            subreddit_mongo = message_enrich.convert_message_for_mongodb(subreddit_elastic)
            try:
                mongo.insert_one(messages.mongodb_db, messages.mongodb_collection_reddit, subreddit_mongo)
            except Exception as err_mongo:
                print(f"Couldn't send reddit id {subreddit['id']} to MongoDB: {str(err_mongo)}")
            try:
                elasticsearch.index(index=messages.elasticsearch_index_reddit, id=subreddit_elastic['id'],
                                    body=subreddit_elastic)
            except Exception as err_elastic:
                print(f"Couldn't send reddit id {subreddit['id']} to ElasticSearch: {str(err_elastic)}")
        except Exception as err:
            print(f"Couldn't read reddit for id {subreddit['id']}: {str(err)}")


def fetch_symbol_reddit_process_async(symbol: str, max_results: int = 10, date: str = None):
    """
    Fetch subreddit-posts and sends them to Kafka
    :param symbol: stock ticker symbol, e.g. TSLA
    :param max_results:
    :param date:
    :return:
    """
    subreddits = reddit.get_symbol_subreddits(symbol, max_results, date)
    if len(subreddits) > 0:
        print(f"Sending {len(subreddits)} subreddits to Kafka")
    for subreddit in subreddits:
        try:
            kafka.send(messages.kafka_topic_reddit_raw, subreddit)
        except Exception as err:
            print("Couldn't read reddit for id " + subreddit['id'] + ": " + str(err))


def fetch_symbol_tweets_process_sync(symbol: str, max_results: int = 10, date: str = None, next_token: str = None):
    """
    Fetches tweets, evaluates the sentiments and sends them to MongoDB/ElasticSearch
    :param symbol: stock ticker symbol, e.g. TSLA
    :param max_results:
    :param date:
    :param next_token:
    :return:
    """
    tweets, metadata = twitter.get_symbol_tweets(symbol, max_results, date, next_token)
    for tweet in tweets:
        try:
            tweet_elastic = message_enrich.enrich_tweet_message_with_sentiment_symbol_date(tweet)
            tweet_mongo = message_enrich.convert_message_for_mongodb(tweet_elastic)
            try:
                mongo.insert_one(messages.mongodb_db, messages.mongodb_collection_tweets, tweet_mongo)
            except Exception as err_mongo:
                print(f"Couldn't send tweets for id {tweet['id']} to MongoDB: {str(err_mongo)}")
            try:
                elasticsearch.index(index=messages.elasticsearch_index_tweets, id=tweet_elastic['id'],
                                    body=tweet_elastic)
            except Exception as err_elastic:
                print(f"Couldn't send tweets for id {tweet['id']} to ElasticSearch: {str(err_elastic)}")
        except Exception as err:
            print(f"Couldn't read tweets for id {tweet['id']}: {str(err)}")
    if 'next_token' in metadata:
        return metadata['next_token']
    return None


def fetch_symbol_tweets_process_async(symbol: str, max_results=10, date: str = None, next_token: str = None):
    """
    Fetch tweets and sends them to Kafka
    :param symbol: stock ticker symbol, e.g. TSLA
    :param max_results:
    :param date:
    :param next_token:
    :return:
    """
    tweets, metadata = twitter.get_symbol_tweets(symbol, max_results, date, next_token)
    if len(tweets) > 0:
        print(f"Sending {len(tweets)} tweets to Kafka")
    for tweet in tweets:
        try:
            kafka.send(messages.kafka_topic_tweets_raw, tweet)
        except Exception as err:
            print(f"Couldn't send tweet width id {tweet['id']} to Kafka: {str(err)}")


def fetch_symbol_chart(symbol: str):
    response = yahoo.get_symbol_daily_chart_for_1year(symbol)
    quotes = yahoo_converters.convert_symbol_daily_chart_to_insert(response)
    sql = 'insert into quote (symbol, date, price_opening, price_closing, price_closing_adjusted, price_high, price_low, volume) values (%s, %s, %s, %s, %s, %s, %s, %s)'
    postgres.execute_with_cursor(lambda cur: cur.execute("delete from quote where symbol = %s", (symbol,)))
    postgres.execute_with_cursor(lambda cur: cur.executemany(sql, quotes))


def fetch_symbol_summary(symbol: str):
    """
    Fetches basic symbol information and sends them to MongoDB
    :param symbol:
    :return:
    """
    summary = yahoo.get_symbol_summary(symbol)
    summary['symbol'] = symbol
    summary['id'] = hash_utils.get_id_hash(symbol)
    summary_mongo = message_enrich.convert_message_for_mongodb(summary)
    mongo.insert_one(messages.mongodb_db, messages.mongodb_collection_summary, summary_mongo)


def fetch_symbol_news_process_sync(symbol: str, snippetCount: int = 20):
    """
    Fetches Yahoo finance news, evaluates the sentiments and sends them to MongoDB/ElasticSearch
    :param symbol: stock ticker symbol, e.g. TSLA
    :param snippetCount:
    :return:
    """
    news_list_response = yahoo.get_symbol_news(symbol, snippetCount)
    uuids = yahoo_converters.convert_news_list_to_uuids(news_list_response)
    for uuid in uuids:
        try:
            fetch_news_details_process_sync(uuid)
        except Exception as err:
            print("Couldn't read news for id " + uuid)
            print(err)


def fetch_symbol_news_process_async(symbol: str, max_results: int = None):
    """
    Fetch Yahoo finance news and sends them to Kafka
    :param symbol: stock ticker symbol, e.g. TSLA
    :param max_results:
    :return:
    """
    news_list_response = yahoo.get_symbol_news(symbol, max_results)
    uuids = yahoo_converters.convert_news_list_to_uuids(news_list_response)
    if len(uuids) > 0:
        print(f"Fetching {len(uuids)} news from Yahoo and sending it to Kafka")
    for uuid in uuids:
        try:
            news_details = yahoo.get_news_details(uuid)
            kafka.send(messages.kafka_topic_news_raw, json.dumps(news_details))
        except Exception as err:
            print(f"Couldn't read news for id {uuid}: {err}")


def fetch_news_details_process_sync(uuid: str):
    """
    Fetches news details and sends them to MongoDB and ElasticSearch.
    The news list service method delivers only a list of UUIDs
    :param uuid: UUID from news list response entry
    :return:
    """
    news = yahoo.get_news_details(uuid)
    news_elastic = message_enrich.enrich_news_message_with_sentiment_symbol_date(news)
    news_mongo = message_enrich.convert_message_for_mongodb(news_elastic)
    try:
        mongo.insert_one(messages.mongodb_db, messages.mongodb_collection_news, news_mongo)
    except Exception as err_mongo:
        print(f"Couldn't send news for id {uuid} to MongoDB: {err_mongo}")
    try:
        elasticsearch.index(index=messages.elasticsearch_index_news, id=uuid, body=news_elastic)
    except Exception as err_elastic:
        print(f"Couldn't send news for id {uuid} to ElasticSearch: {err_elastic}")


additional_aggregations_tweets = {
    'avgRetweetCount': {'$avg': "$public_metrics.retweet_count"},
    'avgReplyCount': {'$avg': "$public_metrics.reply_count"},
    'avgLikeCount': {'$avg': "$public_metrics.like_count"},
    'avgQuoteCount': {'$avg': "$public_metrics.quote_count"},
    'avgFollowersCount': {'$avg': "$author.public_metrics.followers_count"},
    'avgFollowingCount': {'$avg': "$author.public_metrics.following_count"},
    'avgTweetCount': {'$avg': "$author.public_metrics.tweet_count"},
    'avgListedCount': {'$avg': "$author.public_metrics.listed_count"}
}

additional_aggregations_reddit = {
    'avgUps': {'$avg': "$ups"},
    'avgDowns': {'$avg': "$downs"},
    'avgScore': {'$avg': "$score"},
    'avgUpVoteRatio': {'$avg': "$upvote_ratio"},
    'avgNumComments': {'$avg': "$num_comments"},
    'avgSubRedditSubscribers': {'$avg': "$subreddit_subscribers"},
    'avgNumCrossPosts': {'$avg': "$num_crossposts"},
    'avgWls': {'$avg': "$wls"},
    'avgPwls': {'$avg': "$pwls"},
    'avgGilded': {'$avg': "$gilded"}
}


def find_news_sentiments_for_symbol(symbol: str):
    return find_sentiments_for_symbol(symbol, messages.mongodb_collection_news, {})


def find_tweet_sentiments_for_symbol(symbol: str):
    return find_sentiments_for_symbol(symbol, messages.mongodb_collection_tweets, additional_aggregations_tweets)


def find_reddit_sentiments_for_symbol(symbol: str):
    return find_sentiments_for_symbol(symbol, messages.mongodb_collection_reddit, additional_aggregations_reddit)


def find_sentiments_for_symbol(symbol: str, collection: str, group_additional_aggregations={}):
    """
    Fetches aggregated symbol sentiments
    :param symbol: stock ticker symbol, e.g. TSLA
    :param collection:
    :param group_additional_aggregations:
    :return:
    """
    group = {
        '_id': "$date",
        'count': {'$sum': 1},
        'length': {'$avg': '$sentiment.length'},
        'words': {'$avg': '$sentiment.words'},
        'avgNltkPos': {'$avg': "$sentiment.nltk.pos"},
        'avgNltkNeg': {'$avg': "$sentiment.nltk.neg"},
        'avgNltkNeu': {'$avg': "$sentiment.nltk.neu"},
        'avgNltkCompound': {'$avg': "$sentiment.nltk.compound"},
        'avgNltkMillis': {'$avg': "$sentiment.nltk.millis"},
        'avgFlairPos': {'$avg': "$sentiment.flair.pos"},
        'avgFlairNeg': {'$avg': "$sentiment.flair.neg"},
        'avgFlairMillis': {'$avg': "$sentiment.flair.millis"},
        'avgTextblobPolarity': {'$avg': "$sentiment.textblob.polarity"},
        'avgTextblobSubjectivity': {'$avg': "$sentiment.textblob.subjectivity"},
        'avgTextblobMillis': {'$avg': "$sentiment.textblob.millis"}
    }
    group = {**group, **group_additional_aggregations}
    pipeline = [
        {'$match': {'symbol': symbol}},
        {'$group': group},
        {'$sort': {"_id": 1}}
    ]
    aggregation_results = mongo.execute_aggregation(messages.mongodb_db, collection, pipeline)
    return aggregation_results


def find_sentiment_counts_by_symbol(collection: str):
    pipeline = [
        {'$group': {
            '_id': '$symbol',
            'count': {'$sum': 1}}},
        {'$sort': {'_id': 1}}
    ]
    aggregation_results = mongo.execute_aggregation(config.get_mongodb_db(), collection, pipeline)
    return aggregation_results


def find_symbols_tweets_for_query(symbol: str, query_string: str = "", size=10, pre_tag="[", post_tag="]"):
    fields = ["symbol", "date", "author.username"]
    query = create_elasticsearch_query(symbol, query_string, "text", size, fields, pre_tag, post_tag)
    result = elasticsearch.search(index=messages.elasticsearch_index_tweets, body=query)
    return result


def find_symbols_reddit_for_query(symbol: str, query_string: str = "", size=10, pre_tag="[", post_tag="]"):
    fields = ["symbol", "date", "author_fullname"]
    query = create_elasticsearch_query(symbol, query_string, "selftext", size, fields, pre_tag, post_tag)
    result = elasticsearch.search(index=messages.elasticsearch_index_reddit, body=query)
    return result


def find_symbols_news_for_query(symbol: str, query_string: str = "", size=10, pre_tag="[", post_tag="]"):
    fields = ["symbol", "date", "data.contents.content.authors.author"]
    query = create_elasticsearch_query(symbol, query_string, "data.contents.content.summary",
                                       size, fields, pre_tag, post_tag)
    result = elasticsearch.search(index=messages.elasticsearch_index_news, body=query)
    return result


def convert_elasticsearch_results_to_dataframe(result):
    records = convert_elasticsearch_results_list(result)
    df = pd.DataFrame(records)
    if records and len(records) > 0:
        return df.set_index('id')
    return df


def convert_elasticsearch_results_to_html(result):
    records = convert_elasticsearch_results_list(result)
    html = dict_list_html_converter.convert_dict_list_to_html(records)
    return html


def convert_elasticsearch_results_list(result):
    """
    Converts the ElasticSearch response object in a dictionary list
    :param result:
    :return:
    """
    if not 'hits' in result: return []
    outer_hits = result['hits']
    if not 'hits' in outer_hits: return []
    hits = outer_hits['hits']
    records = []
    for hit in hits:
        fields = hit['fields']
        highlight = hit['highlight']
        metadata = {'id': hit['_id'], 'score': hit['_score']}
        record = {**metadata, **fields, **highlight}
        records.append(record)
    return records


HTML_PRE_TAG = "<strong style=\"color:red;\">"
HTML_POST_TAG = "</strong>"


def create_elasticsearch_query(symbol: str, query_string: str, text_field: str, size: int = 10,
                               fields: [str] = [], pre_tag="[", post_tag="]"):
    """
    Creates ElasticSearch query for Twitter/Reddit/News index
    :param symbol: stock ticker symbol, e.g. TSLA
    :param query_string:
    :param text_field:
    :param size:
    :param fields: response fields (in addition to highlighted text_field)
    :param pre_tag: sampling text hit marker start
    :param post_tag: sampling text hit marker end
    :return:
    """
    if not size or size < 1: size = 1
    query = {
        "from": 0,
        "size": size,
        "query": {
            "bool": {
                "must": {
                    "query_string": {
                        "query": query_string,
                        "default_field": text_field,
                        "default_operator": "AND"
                    }
                },
                "filter": {
                    "match": {
                        "symbol": symbol
                    }
                }
            }
        },
        "highlight": {
            "number_of_fragments": 3,
            "fragment_size": 500,
            "fragmenter": "simple",
            "fields": {
                text_field: {
                    "pre_tags": [pre_tag], "post_tags": [post_tag]
                }
            }
        }
    }
    if fields and len(fields) > 0:
        query['fields'] = fields
    return query

import util.process_utils as pu
import util.rabbitmq_utils as rabbitmq
import service.financial_services as financial_services
import json


class SymbolActionWorker:
    def callback(self, channel, method_frame, properties, body):
        pu.exec_and_log_pipeline(lambda: self.process_message(channel, method_frame, properties, body),
                                 f"Message: RabbitMQ (symbol-action)")

    def process_message(self, channel, method_frame, properties, body):
        try:
            if body:
                s_body = body.decode("utf-8")
                document = json.loads(s_body)
                symbol = document['symbol']
                action = document['action']
                date = None
                max_results = None
                if 'date' in document:
                    date = document['date']
                if 'max_results' in document:
                    max_results = document['max_results']
                self.exec_symbol_action(symbol, action, max_results, date)
        except Exception as err:
            print(err)
            # channel.basic_nack(delivery_tag=method_frame.delivery_tag)
        channel.basic_ack(delivery_tag=method_frame.delivery_tag)

    def exec_symbol_action(self, symbol: str, action: str, max_results: int = None, date: str = None):
        if action == 'chart':
            financial_services.fetch_symbol_chart(symbol)
        elif action == 'summary':
            financial_services.fetch_symbol_summary(symbol)
        elif action == 'news':
            financial_services.fetch_symbol_news_process_async(symbol, max_results)
        elif action == 'tweets':
            financial_services.fetch_symbol_tweets_process_async(symbol, max_results, date)
        elif action == 'reddit':
            financial_services.fetch_symbol_reddit_process_async(symbol, max_results, date)

    def start_worker(self):
        worker = rabbitmq.create_worker('symbol-action', self.callback)
        pu.endless_loop(worker, "SymbolActionWorker")

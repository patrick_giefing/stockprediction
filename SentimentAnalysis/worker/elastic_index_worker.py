import json
from elasticsearch import Elasticsearch
import util.process_utils as pu
import util.rabbitmq_utils as rabbitmq
import config.config as config


class ElasticIndexWorker:
    def __init__(self, queue: str, index: str):
        self.es = Elasticsearch(hosts=config.get_elasticsearch_host())
        self.queue = queue
        self.index = index

    def callback(self, channel, method_frame, properties, body):
        pu.exec_and_log_pipeline(lambda: self.process_message(channel, method_frame, properties, body),
                                 f"Message: RabbitMQ ({self.queue}) -> ElasticSearch ({self.index})")

    def process_message(self, channel, method_frame, properties, body):
        print(f"Message: RabbitMQ ({self.queue}) -> ElasticSearch ({self.index})")
        message = json.loads(body.decode("utf-8"))
        self.es.index(index=self.index, id=message['id'], body=message)
        channel.basic_ack(delivery_tag=method_frame.delivery_tag)

    def start_worker(self):
        worker = rabbitmq.create_worker(self.queue, self.callback)
        pu.endless_loop(worker, f"ElasticSearchIndexer ({self.queue} -> {self.index})")

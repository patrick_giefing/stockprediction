import threading
from worker.elastic_index_worker import ElasticIndexWorker
from worker.mongo_db_indexer import MongoDbIndexWorker
import config.messages as messages
from worker.sentiment_analysis_worker import SentimentAnalysisWorker
from worker.symbol_action_worker import SymbolActionWorker
import message.message_enrich as message_enrich


class WorkerThread(threading.Thread):
    def __init__(self, thread_id, func):
        threading.Thread.__init__(self)
        self.threadID = thread_id
        self.func = func

    def run(self):
        print("Starting " + self.name)
        self.func()
        print("Exiting " + self.name)


def create_workers():
    symbol_action_worker = WorkerThread(1, lambda: SymbolActionWorker().start_worker())
    elastic_index_worker_tweets = \
        WorkerThread(11,
                     lambda: ElasticIndexWorker(
                         messages.rabbitmq_queue_tweet_elasticsearch,
                         messages.elasticsearch_index_tweets).start_worker())
    elastic_index_worker_news = \
        WorkerThread(12,
                     lambda: ElasticIndexWorker(
                         messages.rabbitmq_queue_news_elasticsearch,
                         messages.elasticsearch_index_news).start_worker())
    elastic_index_worker_reddit = \
        WorkerThread(13,
                     lambda: ElasticIndexWorker(
                         messages.rabbitmq_queue_reddit_elasticsearch,
                         messages.elasticsearch_index_reddit).start_worker())
    mongodb_worker_tweets = \
        WorkerThread(21,
                     lambda: MongoDbIndexWorker(
                         messages.rabbitmq_queue_tweet_mongodb,
                         messages.mongodb_collection_tweets).start_worker())
    mongodb_worker_news = \
        WorkerThread(22,
                     lambda: MongoDbIndexWorker(
                         messages.rabbitmq_queue_news_mongodb,
                         messages.mongodb_collection_news).start_worker())
    mongodb_worker_reddit = \
        WorkerThread(23,
                     lambda: MongoDbIndexWorker(
                         messages.rabbitmq_queue_reddit_mongodb,
                         messages.mongodb_collection_reddit).start_worker())
    sentiment_analysis_worker_tweets = \
        WorkerThread(31,
                     lambda: SentimentAnalysisWorker(
                         messages.kafka_topic_tweets_raw,
                         messages.rabbitmq_exchange_tweet,
                         message_enrich.enrich_tweet_message_with_sentiment_symbol_date).start_worker())
    sentiment_analysis_worker_news = \
        WorkerThread(32,
                     lambda: SentimentAnalysisWorker(
                         messages.kafka_topic_news_raw,
                         messages.rabbitmq_exchange_news,
                         message_enrich.enrich_news_message_with_sentiment_symbol_date).start_worker())
    sentiment_analysis_worker_reddit = \
        WorkerThread(33,
                     lambda: SentimentAnalysisWorker(
                         messages.kafka_topic_reddit_raw,
                         messages.rabbitmq_exchange_reddit,
                         message_enrich.enrich_reddit_message_with_sentiment_symbol_date).start_worker())

    return [symbol_action_worker,
            elastic_index_worker_tweets, elastic_index_worker_news, elastic_index_worker_reddit,
            mongodb_worker_tweets, mongodb_worker_news, mongodb_worker_reddit,
            sentiment_analysis_worker_tweets, sentiment_analysis_worker_news, sentiment_analysis_worker_reddit]


def create_and_start_workers():
    workers = create_workers()
    [w.start() for w in workers]
    print("Exiting Main Thread")


if __name__ == '__main__':
    create_and_start_workers()

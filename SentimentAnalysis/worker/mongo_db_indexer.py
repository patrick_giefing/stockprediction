import json
import util.process_utils as pu
import util.rabbitmq_utils as rabbitmq
import util.mongodb_utils as mongodb
import config.config as config


class MongoDbIndexWorker:
    def __init__(self, queue: str, collection: str):
        self.queue = queue
        self.collection = collection

    def callback(self, channel, method_frame, properties, body):
        pu.exec_and_log_pipeline(lambda: self.process_message(channel, method_frame, properties, body),
                                 f"Message: RabbitMQ ({self.queue}) -> MongoDB ({self.index})")

    def process_message(self, channel, method_frame, properties, body):
        if body:
            s_body = body.decode("utf-8")
            document = json.loads(s_body)
            mongo_client = mongodb.create_mongodb_client()
            mongo_finance = mongo_client[config.get_mongodb_db()]
            mongo_tweets = mongo_finance[self.collection]
            mongo_tweets.insert_one(document)
        channel.basic_ack(delivery_tag=method_frame.delivery_tag)

    def start_worker(self):
        worker = rabbitmq.create_worker(self.queue, self.callback)
        pu.endless_loop(worker, "MongoDbIndexer")

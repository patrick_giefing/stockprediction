import util.kafka_utils as kafka
import util.rabbitmq_utils as rabbitmq
import util.process_utils as pu
import json


class SentimentAnalysisWorker:
    def __init__(self, topic: str, exchange: str, func_enrich_index_object):
        self.topic = topic
        self.exchange = exchange
        self.func_enrich_index_object = func_enrich_index_object

    def work(self):
        consumer = kafka.create_json_consumer(self.topic, 'sentiment-analysis')
        connection = rabbitmq.create_connection()
        channel = connection.channel()
        for message in consumer:
            json_message = message.value
            if json_message:
                try:
                    pu.exec_and_log_pipeline(lambda: self.process_message(json_message, channel),
                                             f"Message: Kafka ({self.topic}) -> RabbitMQ ({self.exchange})")
                except:
                    pass

    def process_message(self, json_message, channel):
        message = json.loads(json_message)
        message = self.func_enrich_index_object(message)
        str_message = json.dumps(message)
        channel.basic_publish(exchange=self.exchange, routing_key='all', body=str_message)

    def start_worker(self):
        pu.endless_loop(self.work, "Sentiment analysis")

import config.config_data as config_data
import config.rapidapi_config_data as rapidapi
import config.twitter_config_data as twitter
import config.postgres_config_data as postgres
import config.reddit_config_data as reddit


def get_elasticsearch_host() -> str:
    return throw_if_empty(config_data.elasticsearch_host, 'elasticsearch_host')


def get_postgres_db() -> str:
    return throw_if_empty(postgres.postgres_db, 'postgres_db')


def get_postgres_host() -> str:
    return throw_if_empty(postgres.postgres_host, 'postgres_host')


def get_postgres_port() -> int:
    return throw_if_empty(postgres.postgres_port, 'postgres_port')


def get_postgres_user() -> str:
    return throw_if_empty(postgres.postgres_user, 'postgres_user')


def get_postgres_password() -> str:
    return throw_if_empty(postgres.postgres_password, 'postgres_password')


def get_twitter_api_key() -> str:
    return throw_if_empty(twitter.apiKey, 'twitter_api_key')


def get_twitter_api_key_secret() -> str:
    return throw_if_empty(twitter.apiKeySecret, 'twitter_api_key_secret')


def get_twitter_bearer_token() -> str:
    return throw_if_empty(twitter.bearerToken, 'twitter_bearer_token')


def get_twitter_access_token() -> str:
    return throw_if_empty(twitter.accessToken, 'twitter_access_token')


def get_twitter_access_token_secret() -> str:
    return throw_if_empty(twitter.accessTokenSecret, 'twitter_access_token_secret')


def get_rapidapi_key() -> str:
    return throw_if_empty(rapidapi.rapidApiKey, 'rapid_api_key')


def get_kafka_brokers() -> [str]:
    return config_data.kafka_brokers


def get_rabbitmq_user() -> str:
    return throw_if_empty(config_data.rabbitmq_user, 'rabbitmq_user')


def get_rabbitmq_password() -> str:
    return throw_if_empty(config_data.rabbitmq_password, 'rabbitmq_password')


def get_rabbitmq_host() -> str:
    return throw_if_empty(config_data.rabbitmq_host, 'rabbitmq_host')


def get_rabbitmq_port() -> int:
    return throw_if_empty(config_data.rabbitmq_port, 'rabbitmq_port')


def get_rabbitmq_virtual_host() -> str:
    return throw_if_empty(config_data.rabbitmq_virtual_host, 'rabbitmq_virtual_host')


def get_mongodb_url() -> str:
    return throw_if_empty(config_data.mongodb_url, 'mongodb_url')


def get_mongodb_db() -> str:
    return throw_if_empty(config_data.mongodb_db, 'mongodb_db')


def get_reddit_client_id() -> str:
    return throw_if_empty(reddit.client_id, 'reddit_client_id')


def get_reddit_client_secret() -> str:
    return throw_if_empty(reddit.client_secret, 'reddit_client_secret')


def throw_if_empty(value, key: str):
    if not value or str(value) == "":
        raise LookupError("Config value not set: " + key)
    return value

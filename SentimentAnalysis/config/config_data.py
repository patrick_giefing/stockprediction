kafka_brokers = ['kafka01:9092']

mongodb_host = "mongodb01"
mongodb_port = 27017
mongodb_user = "mongouser"
mongodb_password = "1234567"
mongodb_url = f"mongodb://{mongodb_user}:{mongodb_password}@{mongodb_host}:{mongodb_port}/"

rabbitmq_user = 'rabbituser'
rabbitmq_password = '1234567'
rabbitmq_host = 'rabbitmq'
rabbitmq_port = 5672
rabbitmq_virtual_host = '/'

elasticsearch_host = 'elasticsearch'
import util.sentiments as sentiments
import datetime
import util.date_utils as date_utils
import copy
import rest.client.extractor.yahoo_finance_response_extractors as yahoo_extractors
from bson.objectid import ObjectId
import re


def enrich_tweet_message_with_sentiment_symbol_date(tweet) -> ({}, {}):
    tweet = copy.deepcopy(tweet)
    text = tweet['text']
    sentiment = sentiments.sentiments(text)
    date = date_utils.get_date_string_from_string(tweet["created_at"])
    tweet['sentiment'] = sentiment
    tweet['date'] = date
    return tweet


def enrich_news_message_with_sentiment_symbol_date(news):
    news = copy.deepcopy(news)
    text = news['data']['contents'][0]['content']['body']['markup']
    sentiment = sentiments.sentiments(text)
    symbols = yahoo_extractors.get_news_details_symbols(news)
    date = yahoo_extractors.get_news_details_date(news)
    news['sentiment'] = sentiment
    news['symbol'] = symbols
    news['date'] = date
    news['id'] = news['data']['contents'][0]['content']['id']
    return news


def enrich_reddit_message_with_sentiment_symbol_date(reddit):
    reddit = copy.deepcopy(reddit)
    text = reddit['selftext']
    sentiment = sentiments.sentiments(text)
    date = datetime.datetime.fromtimestamp(reddit["created"])
    date = date.strftime('%Y-%m-%d')
    reddit['sentiment'] = sentiment
    reddit['date'] = date
    return reddit


def convert_message_for_mongodb(message):
    """
    Some justifications are necessary for MongoDB:
    Date must be a datetime instead of a string.
    ID must be an ObjectId.
    :param message:
    :return:
    """
    message = copy.deepcopy(message)
    if 'date' in message:
        date = message['date']
        if date:
            message['date'] = datetime.datetime.strptime(date, '%Y-%m-%d')
    if 'id' in message:
        id = message['id']
        if id:
            message['_id'] = uuid_to_object_id(id)
    return message


regex = re.compile(r"[^a-f0-9]", re.IGNORECASE)


def uuid_to_object_id(uuid) -> ObjectId:
    """
    Converts a possible uuid to an ObjectId. Non-hex characters are removed and the remaining string multiplied
    to hopefully get a result string which is at least 24 characters long. If it's longer, only the first 24 characters
    are taken. This may not be useful for production but it's the easiest way to always get the same ObjectId from
    an input uuid.
    :param uuid:
    :return:
    """
    uuid = str(uuid).lower()
    uuid = regex.sub('', uuid)
    if len(uuid) > 1:
        object_id = (uuid * 24)[0:24]
        return ObjectId(object_id)
    return ObjectId()

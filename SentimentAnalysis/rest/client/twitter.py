import util.rest_utils as rest_utils
import config.config as config
import requests
import rest.client.converter.twitter_response_converters as twitter_response_converters


def get_symbol_tweets(symbol: str, max_results: int = 10, date: str = None, next_token: str = None):
    """

    :param symbol:
    :param max_results:
    :param date: 2019-10-12T07:20:50.52Z - The oldest UTC timestamp (from most recent 7 days) from which the Tweets will be provided. YYYY-MM-DDTHH:mm:ssZ (ISO 8601/RFC 3339).
    :param next_token:
    :return:
    """
    if not max_results: max_results = 10
    max_results = validate_and_correct_max_results(max_results)
    url = 'https://api.twitter.com/2/tweets/search/recent'
    querystring = {"query": "#" + symbol, "max_results": max_results,
                   "tweet.fields": "attachments,context_annotations,created_at,entities,geo,id,lang,public_metrics,reply_settings,source,text,withheld",
                   "user.fields": "public_metrics",
                   "expansions": "author_id",
                   "place.fields": "geo"}
    if date and len(date) == 10:
        querystring['start_time'] = f'{date}T13:00:00.00Z'
    if next_token:
        querystring['next_token'] = next_token
    headers = create_twitter_headers()
    response = requests.request("GET", url, headers=headers, params=querystring)
    tweet_dict = rest_utils.get_dict_from_json_response(response)
    if 'errors' in tweet_dict:
        errors = tweet_dict['errors']
        if len(errors) > 0:
            raise Exception(str(errors[0]))
    tweet_dict = twitter_response_converters.enrich_search_response_with_author_data(tweet_dict)
    tweets = tweet_dict["data"]
    for tweet in tweets:
        tweet['symbol'] = symbol
    return tweets, tweet_dict["meta"]


def validate_and_correct_max_results(max_results: int) -> int:
    """[{'parameters': {'max_results': ['5']}, 'message': 'The `max_results` query parameter value [5] is not between 10 and 100'}]"""
    if max_results < 10: return 10
    if max_results > 100: return 100
    return max_results


def create_twitter_headers():
    headers = {
        'Authorization': 'Bearer ' + config.get_twitter_bearer_token(),
    }
    return headers

import util.date_utils as date_utils


def get_news_details_symbols(dict) -> [str]:
    tickers = dict["data"]["contents"][0]["content"]["finance"]["stockTickers"]
    symbols = [d["symbol"] for d in tickers]
    return symbols


def get_news_details_date(dict) -> str:
    pub_date: str = dict["data"]["contents"][0]["content"]["pubDate"]
    date = date_utils.get_date_string_from_string(pub_date)
    return date

import datetime


def convert_symbol_daily_chart_to_insert(symbol_chart):
    chart = symbol_chart['chart']
    if not chart: return None
    result = chart['result']
    if not result or len(result) == 0: return None
    symbol_result = result[0]
    meta = symbol_result['meta']
    symbol = meta['symbol']
    timestamps = symbol_result['timestamp']
    indicators = symbol_result['indicators']
    quotes = indicators['quote'][0]
    prices_close_adjusted = indicators['adjclose'][0]['adjclose']
    prices_open = quotes['open']
    prices_close = quotes['close']
    volumes = quotes['volume']
    prices_low = quotes['low']
    prices_high = quotes['high']
    values = []
    for index in range(len(timestamps)):
        value = (symbol, datetime.datetime.fromtimestamp(timestamps[index]), prices_open[index], prices_close[index],
                 prices_close_adjusted[index], prices_high[index], prices_low[index], volumes[index])
        values.append(value)
    return values


def convert_news_list_to_uuids(symbol_news_list):
    objects = symbol_news_list['data']['main']['stream']
    uuids = [o['id'] for o in objects]
    return uuids

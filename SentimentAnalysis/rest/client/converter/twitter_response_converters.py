def get_user_dict_from_search_response(search_response):
    if not "includes" in search_response: return {}
    includes = search_response["includes"]
    if not "users" in includes: return {}
    users = includes["users"]
    users_dict = {}
    for user in users:
        user_id = user['id']
        users_dict[user_id] = user
    return users_dict


def enrich_search_response_with_author_data(search_response):
    user_dict = get_user_dict_from_search_response(search_response)
    tweets = search_response['data']
    for tweet in tweets:
        if not "author_id" in tweet: continue
        author_id = tweet['author_id']
        if author_id in user_dict:
            author = user_dict[author_id]
            tweet['author'] = author
    return search_response

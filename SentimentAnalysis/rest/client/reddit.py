import praw
import config.config as config
import util.date_utils as date_utils


def get_symbol_subreddits(symbol: str, max_results: int = 10, date: str = None):
    if not max_results: max_results = 10
    posts = []
    date = None  # submissions function seems not to be available anymore
    subreddit = "wallstreetbets"
    query = f"selftext:${symbol}"
    reddit = praw.Reddit(client_id=config.get_reddit_client_id(),
                         client_secret=config.get_reddit_client_secret(),
                         user_agent="praw")
    results = None
    if date and len(date) == 10:
        millis = date_utils.get_millis_from_date_string(date)
        results = reddit.subreddit(subreddit).submissions(start=millis, extra_query=query)
    else:
        results = reddit \
            .subreddit(subreddit) \
            .search(query, sort="new", time_filter="all", limit=max_results)

    for result in results:
        v = vars(result)
        v['_reddit'] = None
        v['subreddit'] = None
        v['author'] = None
        v['symbol'] = symbol
        if 'edited' in v:
            v['edited'] = None
        posts.append(v)
    return posts

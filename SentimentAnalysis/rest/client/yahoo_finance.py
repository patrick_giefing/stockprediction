import config.config as config
import requests
import json
from types import SimpleNamespace
import util.rest_utils as rest_utils


def get_symbol_daily_chart_for_1year(symbol: str):
    url = "https://apidojo-yahoo-finance-v1.p.rapidapi.com/stock/v2/get-chart"
    querystring = {"interval": "1d", "symbol": symbol, "range": "1y", "region": "US"}
    headers = create_rapidapi_headers()
    response = requests.request("GET", url, headers=headers, params=querystring)
    return rest_utils.get_dict_from_json_response(response)


def get_symbol_summary(symbol: str):
    url = "https://apidojo-yahoo-finance-v1.p.rapidapi.com/stock/v2/get-summary"
    querystring = {"symbol": symbol, "region": "US"}
    headers = create_rapidapi_headers()
    response = requests.request("GET", url, headers=headers, params=querystring)
    return rest_utils.get_dict_from_json_response(response)


def get_symbol_news(symbol: str, max_results: int = 10):
    if not max_results: max_results = 10
    url = "https://apidojo-yahoo-finance-v1.p.rapidapi.com/news/v2/list"
    querystring = {"region": "US", "snippetCount": max_results, "s": symbol}
    payload = "Pass in the value of uuids field returned right in this endpoint to load the next page, or leave empty to load first page"
    headers = create_rapidapi_headers()
    response = requests.request("POST", url, data=payload, headers=headers, params=querystring)
    return rest_utils.get_dict_from_json_response(response)


def get_news_details(uuid: str):
    url = "https://apidojo-yahoo-finance-v1.p.rapidapi.com/news/v2/get-details"
    querystring = {"uuid": uuid, "region": "US"}
    headers = create_rapidapi_headers()
    response = requests.request("GET", url, headers=headers, params=querystring)
    return rest_utils.get_dict_from_json_response(response)


def create_rapidapi_headers():
    headers = {
        'x-rapidapi-key': config.get_rapidapi_key(),
        'x-rapidapi-host': "apidojo-yahoo-finance-v1.p.rapidapi.com"
    }
    return headers


def get_json_object_from_response(response):
    json_response = response.text
    object = json.loads(json_response, object_hook=lambda d: SimpleNamespace(**d))
    return object

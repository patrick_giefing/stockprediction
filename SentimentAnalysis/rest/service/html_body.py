def create_with_body(body: str) -> str:
    """
    Create a HTML for the given body element.
    :param body:
    :return:
    """
    html = f'<html>'
    html += '''<head><style type="text/css">
        table, td {border: 1px solid black;}
        thead td {background-color: #efefef;}
    </style></head>'''
    html += f'<body>{body}</body>'
    html += f'</html>'
    return html
def convert_dict_list_to_html(dict_list):
    records = dict_list
    if not records or len(records) == 0: return None
    s = '<table><thead><tr>'
    for k in records[0]: s += f'<td>{k}</td>'
    s += '</tr></thead><tbody>'
    for record in records:
        s += '<tr>'
        for k in record: s += f'<td>{record[k]}</td>'
        s += '</tr>'
    s += '</tbody></table>'
    return s
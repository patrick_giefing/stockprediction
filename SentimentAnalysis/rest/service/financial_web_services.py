# pip install flask
from flask import Flask, request
import service.financial_services as financial_services
import rest.service.html_body as html_body
import rest.service.dict_list_html_converter as dict_list_html_converter

app = Flask(__name__)


@app.route('/exec-symbol-action')
def exec_symbol_action():
    symbol = request.args.get('symbol')
    action = request.args.get('action')
    financial_services.queue_symbol_action(symbol, action)
    return 'true'


@app.route('/sentiments')
def find_symbols_sentiments():
    symbol = request.args.get('symbol')
    collection = request.args.get('collection')
    if collection == 'tweets':
        sentiments = financial_services.find_tweet_sentiments_for_symbol(symbol)
    elif collection == 'reddit':
        sentiments = financial_services.find_reddit_sentiments_for_symbol(symbol)
    else:
        sentiments = financial_services.find_news_sentiments_for_symbol(symbol)
    records = list(sentiments)
    body = dict_list_html_converter.convert_dict_list_to_html(records)
    html = html_body.create_with_body(body)
    return html


@app.route('/find')
def find_symbols_tweets_for_query():
    symbol = request.args.get('symbol')
    query = request.args.get('query')
    index = request.args.get('index')
    if index == 'tweets':
        results = financial_services.find_symbols_tweets_for_query(symbol, query, 10,
                                                                   financial_services.HTML_PRE_TAG,
                                                                   financial_services.HTML_POST_TAG)
    elif index == 'reddit':
        results = financial_services.find_symbols_reddit_for_query(symbol, query, 10,
                                                                   financial_services.HTML_PRE_TAG,
                                                                   financial_services.HTML_POST_TAG)
    else:
        results = financial_services.find_symbols_news_for_query(symbol, query, 10,
                                                                 financial_services.HTML_PRE_TAG,
                                                                 financial_services.HTML_POST_TAG)
    body = financial_services.convert_elasticsearch_results_to_html(results)
    html = html_body.create_with_body(body)
    return html


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=680)

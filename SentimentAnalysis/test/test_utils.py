import json
from types import SimpleNamespace

def get_json_as_dict_from_file(file: str):
    with open(file, "r", encoding="UTF-8") as read_file:
        object = json.load(read_file)
        return object


def get_json_as_object_from_file(file: str):
    with open(file, "r") as read_file:
        object = json.load(read_file, object_hook=lambda d: SimpleNamespace(**d))
        return object

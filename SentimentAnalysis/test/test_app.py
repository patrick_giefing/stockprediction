import service.financial_services as financial_services
import traceback
import pandas as pd
import config.config as config

symbols = {'INTC', 'AAPL', 'CAT', 'NKE', 'MCD', 'MMM', 'PG', 'DIS', 'WMT',
           'CRM', 'VZ', 'MSFT', 'KO', 'JNJ', 'CSCO', 'BA', 'AXP', 'TSLA'}

# SYNC

# print(pd.DataFrame(list(financial_services.find_reddit_sentiments_for_symbol('MSFT'))))


for symbol in symbols:
    try:
        # financial_services.fetch_symbol_reddit_process_sync(symbol, 10000, '2021-01-10')
        # financial_services.fetch_symbol_summary(symbol)
        # financial_services.fetch_symbol_chart(symbol)
        # financial_services.fetch_symbol_news_process_sync(symbol, snippetCount=1000)
        dates = ['2021-01-13', '2021-01-14', '2021-01-15', '2021-01-16', '2021-01-17', '2021-01-18', '2021-01-19',
                 '2021-01-20']
        for date in dates:
            try:
                next_token = None
                for i in range(50):
                    next_token = financial_services.fetch_symbol_tweets_process_sync(symbol, 100, date, next_token)
                    if not next_token:
                        break
            except Exception as err:
                print(f'Error reading tweets for {symbol} and date {date}: {str(err)}')
    except Exception as err_elastic:
        print(err_elastic)
        traceback.print_exc()

# print(pd.DataFrame(list(financial_services.find_sentiments_for_symbol('AMRN'))))
# financial_services.fetch_symbol_summary('TSLA')

# ASYNC

# financial_services.queue_symbol_action('AMRN', 'chart')
# financial_services.queue_symbol_action('AMRN', 'summary')
# financial_services.queue_symbol_action('AMRN', 'news')
# financial_services.queue_symbol_action('AMRN', 'tweets')

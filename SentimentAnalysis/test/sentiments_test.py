import util.sentiments as sentiments
import unittest


def create_and_print_sentiment(text: str):
    sentiment = sentiments.sentiments(text)
    print("Text:   " + text + "\r\nScores: " + str(sentiment))
    return sentiment


class SentimentsTests(unittest.TestCase):
    def test_neutral(self):
        sentiment = create_and_print_sentiment("")
        self.assertLess(sentiment['nltk']['neg'], .001)
        self.assertLess(sentiment['nltk']['pos'], .001)
        self.assertLess(sentiment['textblob']['polarity'], .001)
        self.assertLess(sentiment['textblob']['subjectivity'], .001)

    def test_negative(self):
        sentiment = create_and_print_sentiment("This is a sentence with many very negative words")
        self.assertTrue(.35 <= sentiment['nltk']['neg'] <= .4)
        self.assertTrue(.11 <= sentiment['nltk']['pos'] <= .12)

    def test_positive(self):
        sentiment = create_and_print_sentiment(
            "This is a sentence with many very positive words like cats and dogs and flowers and stuff")
        self.assertTrue(.16 <= sentiment['nltk']['pos'] <= .38)
        self.assertLess(sentiment['nltk']['neg'], .01)

    def test_loremipsum(self):
        sentence = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet."
        sentiment = create_and_print_sentiment(sentence)
        self.assertTrue(sentiment['nltk']['neu'] > .95)


if __name__ == '__main__':
    unittest.main()

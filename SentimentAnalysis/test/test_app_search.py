import service.financial_services as financial_services

print(financial_services.convert_elasticsearch_results_to_dataframe(financial_services.find_symbols_tweets_for_query('TSLA', 'Musk')))
print("\r\n\r\n")
print(financial_services.convert_elasticsearch_results_to_dataframe(financial_services.find_symbols_reddit_for_query('TSLA', 'Musk')))
print("\r\n\r\n")
print(financial_services.convert_elasticsearch_results_to_dataframe(financial_services.find_symbols_news_for_query('KO', 'article')))
import rest.client.yahoo_finance as yahoo
import unittest


class YahooFinanceTests(unittest.TestCase):
    def test_symbol_daily_chart_amrn(self):
        response = yahoo.get_daily_chart_for_1year('AMRN')
        self.assertIsNotNone(response.chart)
        self.assertIsNone(response.error)

    def test_symbol_summary_amrn(self):
        response = yahoo.get_symbol_summary('AMRN')
        self.assertEqual('AMRN', response.symbol)


if __name__ == '__main__':
    unittest.main()

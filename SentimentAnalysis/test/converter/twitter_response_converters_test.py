import unittest
import rest.client.converter.twitter_response_converters as twitter_response_converters
import test.test_utils as test_utils


class YahooFinanceResponseConvertersTests(unittest.TestCase):
    def test_convert_symbol_news_get_uuids(self):
        dict = test_utils.get_json_as_dict_from_file("twitter-search-response-TSLA.json")
        data_anriched = twitter_response_converters.enrich_search_response_with_author_data(dict)
        first_tweet_author = data_anriched['data'][0]['author']
        self.assertIsNotNone(first_tweet_author)


if __name__ == '__main__':
    unittest.main()

import unittest
import rest.client.converter.yahoo_finance_response_converters as yahoo_converters
import test.test_utils as test_utils
import util.postgres_utils as postgres


class YahooFinanceResponseConvertersTests(unittest.TestCase):
    def delete_test_symbol(self):
        postgres.execute_with_cursor(lambda cur: cur.execute("delete from quote where symbol = %s", ("TEST",)))

    def test_convert_symbol_daily_chart_to_sql_test_and_insert_into_db(self):
        self.delete_test_symbol()
        json = test_utils.get_json_as_dict_from_file("chart-response-TEST.json")
        quotes = yahoo_converters.convert_symbol_daily_chart_to_insert(json)
        self.assertEqual(5, len(quotes))
        sql = 'insert into quote (symbol, date, price_opening, price_closing, price_closing_adjusted, price_high, price_low, volume) values (%s, %s, %s, %s, %s, %s, %s, %s)'
        postgres.execute_with_cursor(lambda cur: cur.executemany(sql, quotes))
        self.delete_test_symbol()

    def test_convert_symbol_news_get_uuids(self):
        json = test_utils.get_json_as_dict_from_file("news-list-respone-AMRN.json")
        uuids = yahoo_converters.convert_news_list_to_uuids(json)
        self.assertTrue(len(uuids) > 5)


if __name__ == '__main__':
    unittest.main()

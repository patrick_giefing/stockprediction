import rest.client.twitter as twitter
import unittest


class TwitterTests(unittest.TestCase):
    def test_symbol_daily_chart_amrn(self):
        tweets, metadata = twitter.get_symbol_tweets('AMRN')
        self.assertGreater(1, len(tweets))


if __name__ == '__main__':
    unittest.main()

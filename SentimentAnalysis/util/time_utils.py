import time


def get_current_time_millis():
    return int(round(time.time() * 1000))


def get_func_result_and_millis(func):
    start = get_current_time_millis()
    result = func()
    end = get_current_time_millis()
    millis = end - start
    return result, millis

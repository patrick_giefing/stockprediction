import hashlib


def get_id_hash(id) -> str:
    return hashlib.md5(id.encode('UTF-8')).hexdigest()

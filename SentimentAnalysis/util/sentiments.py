# pip install spacy
# python -m spacy download en_core_web_sm
# python -m spacy download de_core_news_sm
# pip install nltk
# pip3 install flair
# pip3 install textblob
import nltk
import flair
import util.time_utils as time_utils
from textblob import TextBlob

# https://medium.com/@b.terryjack/nlp-pre-trained-sentiment-analysis-1eb52a9d742c

vader_lexicon_downloaded = False
flair_sentiment = None


def __ensure_lexicons_available():
    global vader_lexicon_downloaded, flair_sentiment
    if not vader_lexicon_downloaded:
        nltk.download('vader_lexicon')
        vader_lexicon_downloaded = True
    if not flair_sentiment:
        flair_sentiment = flair.models.TextClassifier.load('en-sentiment')


def __nltk_sentiment(sentence: str) -> str:
    from nltk.sentiment.vader import SentimentIntensityAnalyzer
    sid = SentimentIntensityAnalyzer()
    scores = sid.polarity_scores(sentence)
    return scores


def __flair_sentiment(sentence: str) -> str:
    s = flair.data.Sentence(sentence)
    flair_sentiment.predict(s)
    labels = s.labels
    scores = {}
    for label in labels:
        value = label.value
        if value == 'NEGATIVE':
            value = 'neg'
        elif value == 'POSITIVE':
            value = 'pos'
        else:
            continue
        scores[value] = label.score
    if not 'pos' in scores:
        scores['pos'] = 0.0
    if not 'neg' in scores:
        scores['neg'] = 0.0
    return scores


def __textblob_sentiment(sentence: str) -> str:
    sentiment_textblob = TextBlob(sentence)
    sentiment = sentiment_textblob.sentiment
    scores = {}
    scores['polarity'] = sentiment.polarity
    scores['subjectivity'] = sentiment.subjectivity
    return scores


def sentiments(text: str):
    __ensure_lexicons_available()
    sentiment = {}
    sentiment['length'] = len(text)
    sentiment['words'] = len(text.split())
    sentiment['nltk'] = __append_time(lambda: __nltk_sentiment(text))
    sentiment['flair'] = __append_time(lambda: __flair_sentiment(text))
    sentiment['textblob'] = __append_time(lambda: __textblob_sentiment(text))
    return sentiment


def __append_time(func):
    dict, millis = time_utils.get_func_result_and_millis(func)
    dict['millis'] = millis
    return dict

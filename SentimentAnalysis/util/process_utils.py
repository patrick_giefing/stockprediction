import time


def endless_loop(func, title: str = "App") -> None:
    print(title + " started")
    str_err_last = None
    while True:
        try:
            func()
        except Exception as err:
            str_err = str(err)
            if str_err == str_err_last:
                print(".", end='')
            else:
                print_empty_line = str_err_last != None
                if print_empty_line:
                    print()
                s_error = str(err)
                if err.args and len(err.args) > 0:
                    s_error += f" ({str(err.args)})"
                print(s_error, end='')
            str_err_last = str_err
        time.sleep(1)


def exec_and_log_pipeline(func, message):
    try:
        func()
        print(f"[OK   ] {message}")
    except Exception as err:
        print(f"[ERROR] {message}")
        raise err

import json

def get_dict_from_json_response(response):
    json_response = response.text
    object = json.loads(json_response)
    return object
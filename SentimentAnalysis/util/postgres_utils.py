import psycopg2
import config.config as config


def execute_with_cursor(func):
    conn = get_postgres_connection()
    try:
        cur = conn.cursor()
        func(cur)
        conn.commit()
        cur.close()
    finally:
        close_postgres_connection(conn)


def get_postgres_connection():
    conn = psycopg2.connect(
        f"dbname='{config.get_postgres_db()}' user='{config.get_postgres_user()}' host='{config.get_postgres_host()}' password='{config.get_postgres_password()}'")
    return conn


def close_postgres_connection(conn):
    try:
        conn.close()
    except:
        pass

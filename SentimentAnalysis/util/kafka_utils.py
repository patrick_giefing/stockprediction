# https://towardsdatascience.com/kafka-python-explained-in-10-lines-of-code-800e3e07dad1
# pip install kafka-python
from kafka import KafkaConsumer, KafkaProducer
import json
import config.config as config


def create_json_consumer(topic: str, consumer_group: str) -> KafkaConsumer:
    consumer = KafkaConsumer(
        topic,
        bootstrap_servers=config.get_kafka_brokers(),
        auto_offset_reset='earliest',
        enable_auto_commit=True,
        group_id=consumer_group,
        value_deserializer=lambda x: json.loads(x.decode("utf-8")) if x else None)
    return consumer


def create_json_producer() -> KafkaProducer:
    producer = KafkaProducer(bootstrap_servers=config.get_kafka_brokers(),
                             value_serializer=lambda x: json.dumps(x).encode("utf-8") if x else None)
    return producer


def send(topic: str, object):
    producer = create_json_producer()
    try:
        return producer.send(topic, object)
    finally:
        producer.close()

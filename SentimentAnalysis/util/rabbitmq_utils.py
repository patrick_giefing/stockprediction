# python -m pip install pika --upgrade
import pika
import config.config as config

credentials = pika.PlainCredentials(config.get_rabbitmq_user(), config.get_rabbitmq_password())
parameters = pika.ConnectionParameters(config.get_rabbitmq_host(), config.get_rabbitmq_port(),
                                       config.get_rabbitmq_virtual_host(), credentials)


def create_connection():
    connection = pika.BlockingConnection(parameters)
    return connection


def publish(exchange: str, routing_key: str, str_message: str):
    connection = create_connection()
    channel = connection.channel()
    channel.basic_publish(exchange=exchange, routing_key=routing_key, body=str_message)
    channel.close()
    connection.close()


def create_worker(queue: str, callback) -> None:
    connection = create_connection()
    channel = connection.channel()
    channel.basic_consume(queue=queue,
                          auto_ack=False,
                          on_message_callback=callback)
    channel.start_consuming()
    print("consumer stopped")

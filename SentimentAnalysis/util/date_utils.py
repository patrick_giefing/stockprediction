import datetime

epoch = datetime.datetime.utcfromtimestamp(0)


def get_date_string_from_string(s):
    if not s: return None
    datetime_parts = s.split('T')
    return datetime_parts[0]


def get_millis_from_date_string(s):
    date = datetime.datetime.strptime(s, '%Y-%m-%d')
    millis = (date - epoch).total_seconds() * 1000
    return millis

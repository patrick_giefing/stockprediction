import pymongo
import config.config as config


def create_mongodb_client():
    mongo_client = pymongo.MongoClient(config.get_mongodb_url())
    return mongo_client


def insert_one(db: str, collection: str, object):
    # index_result = with_collection(db, collection, lambda mongo_collection: mongo_collection.insert_one(object))
    index_result = with_collection(db, collection,
                                   lambda mongo_collection: mongo_collection.replace_one({'_id': object['_id']}, object,
                                                                                         upsert=True))
    return index_result


def execute_aggregation(db: str, collection: str, pipeline):
    aggregation_result = with_collection(db, collection, lambda mongo_collection: mongo_collection.aggregate(pipeline))
    return aggregation_result


def with_collection(db: str, collection: str, func):
    mongo_client = create_mongodb_client()
    mongo_db = mongo_client[db]
    mongo_collection = mongo_db[collection]
    result = func(mongo_collection)
    close_connection(mongo_client)
    return result


def close_connection(conn):
    try:
        conn.close()
    except:
        pass

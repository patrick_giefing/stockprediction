import matplotlib.pyplot as plt
import pandas as pd
import yfinance as yf
import statsmodels.api as sm
from statsmodels.graphics.tsaplots import plot_pacf
from statsmodels.tsa.arima.model import ARIMA
from pmdarima import auto_arima
from pmdarima.arima.utils import ndiffs
from statsmodels.graphics.tsaplots import plot_acf

def get_ticker_dataframe(symbol: str, start: str, end:str):
    data = yf.download(symbol, start=start, end=end)
    df = pd.DataFrame(data)
    return df

def draw_diff_charts(df_ticker):
    df_diff_1 = df_ticker.Close.diff().dropna()
    df_diff_1.plot()
    sm.graphics.tsa.plot_acf(df_diff_1)

    df_diff_2 = df_ticker.Close.diff().diff().dropna()
    df_diff_2.plot()
    sm.graphics.tsa.plot_acf(df_diff_2)

def plot_p_charts(df_ticker):
    diff_p = df_ticker.Close.diff().dropna()
    fig, (ax1, ax2) = plt.subplots(1,2, figsize=(16,8))
    ax1.plot(diff_p)
    ax2.set_ylim(0,1)
    plot_pacf(diff_p, ax=ax2)

def plot_q_charts(df_ticker):
    diff_q = df_ticker.Close.diff().dropna()
    fig, (ax1, ax2) = plt.subplots(1,2, figsize=(16,8))
    ax1.plot(diff_q)
    ax2.set_ylim(0,1)
    plot_acf(diff_q, ax=ax2)

def predict_arima(df_ticker, p, d, q):
    model = ARIMA(df_ticker.Close, order=(p, d, q))
    result = model.fit()
    return result

def split_dataframe_train_test(df_ticker, factor):
    train_size = int(round(len(df_ticker) * factor, 0))
    train = df_ticker[:train_size]
    test = df_ticker[train_size:]
    return train, test

def create_forecast_dataframe(df_train, df_test):
    model_forecast = auto_arima(df_train)
    fitted = model_forecast.fit(df_test)
    fc = fitted.predict(n_periods=len(df_test))
    df_forecast = pd.DataFrame(fc,index = df_test.index)
    last_train_price = df_train.iloc[-1]
    first_forecast_price = df_forecast.iloc[0]
    price_diff = first_forecast_price - last_train_price
    # Vorhersage lückenlos an train anpassen
    df_forecast = df_forecast - price_diff
    return df_forecast

def plot_predicted(symbol, df_train, df_test, df_predicted):
    plt.figure(figsize=(12,5), dpi=100)
    plt.plot(df_train,"b", label="Training")
    plt.plot(df_test,"g", label="Test")
    plt.plot(df_predicted,"r", label="Voraussage")
    plt.title(f"Tatsächlicher Kurs / Vorausgesagter Kurs im Vergleich: {symbol}")
    plt.legend(loc=2, fontsize=8)
    plt.show()

def apply_sentiment_to_price(df, func_apply_sentiment_to_price):
    global price_diff_stacked
    price_diff_stacked = 0
    df['CloseAdjusted'] = df.apply(func_apply_sentiment_to_price, axis = 1)
    return df

def calc_d_and_plot_p_q(df_ticker):
    #draw_diff_charts(df_ticker)
    sm.graphics.tsa.plot_acf(df_ticker["Close"]).show()
    # ermitteln der ARIMA Differenzierungsterme (einfacher als manuell)
    d = ndiffs(df_ticker.Close, test = "adf")
    plot_p_charts(df_ticker)
    plot_q_charts(df_ticker)
    return d

def predict_and_plot(symbol, df_ticker, p, d, q, func_get_sentiment_dataframe, func_apply_sentiment_to_price, print_summary = True):
    arima_result = predict_arima(df_ticker, p, d, q)
    if print_summary: print(arima_result.summary())
    df_ticker_train, df_ticker_test = split_dataframe_train_test(df_ticker, .8)
    df_ticket_forecast = create_forecast_dataframe(df_ticker_train['Close'], df_ticker_test['Close'])
    df_ticket_forecast.columns = ['Close']
    df_sentiments = func_get_sentiment_dataframe()
    df_ticker_sentiment = df_ticket_forecast.join(df_sentiments)
    df_ticker_predicted = apply_sentiment_to_price(df_ticker_sentiment, func_apply_sentiment_to_price)
    plot_predicted(symbol, df_ticker_train['Close'], df_ticker_test['Close'], df_ticker_predicted['CloseAdjusted'])